class AuthController < ApplicationController
  def enter
    req = request.env['omniauth.auth']
    user = User.where(provider: req.provider, uid: req.uid).first 
    
    
    
    #render json: req.extra.raw_info.photo_200 and return
    
    unless user
      token = SecureRandom.urlsafe_base64    
      user =  User.create(name: req.info.name, 
                          provider: req.provider, 
                          uid: req.uid, 
                          token: token,
                          avatar: req.extra.raw_info.photo_200)
    end
    
    cookies.permanent[:id] = user.id
    cookies.permanent[:token] = user.token
      
    redirect_to root_url
  end
  
  def exit
    cookies.delete(:id)
    cookies.delete(:token)
    
    redirect_to root_url
  end
  
end
