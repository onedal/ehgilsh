class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :check_login
  
  
  
  
  def check_login
    
    
    id = cookies[:id]
    token = cookies[:token]
    @current_user = User.where("id = ? AND token = ?", id, token).first
    
  end
  
end
